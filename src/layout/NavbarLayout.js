import React, { useEffect } from "react";
import { Container, Nav, Navbar } from "react-bootstrap";
import { Link, Outlet, useNavigate } from "react-router-dom";
import axiosInstance from "../config/AxiosInstance";

function NavbarLayout() {
    let navigate = useNavigate()
    const getWithExpiry = (key) => {
        const itemStr = localStorage.getItem(key);
        if (!itemStr) {
          return null;
        }
        const item = JSON.parse(itemStr);
        const now = new Date();
    
        if (now.getTime() > item.expiry) {
          localStorage.removeItem(key);
          return localStorage.removeItem(key);
        }
        return item.value;
      };

      const setWithExpiry = (key, value, ttl) => {
        const now = new Date();
    
        const item = {
          value: value,
          expiry: now.getTime() + ttl,
        };
        localStorage.setItem(key, JSON.stringify(item));
      };
    
      useEffect(() => {
        getWithExpiry("token") === null && navigate("/login");
      })

      const refreshToken = () => {
          axiosInstance().post('api/token/refresh/', {
            refresh: true
          }).then((json) => {
            const expiry = JSON.parse(
                Buffer.from(json.data.token.split(".")[1], "base64").toString()
              ).exp;
              const date = new Date(expiry * 1000);
              const now = new Date();
              const diffMs = date - now;
              const diffMinute = Math.round(diffMs / 60000);
              setWithExpiry("token", json.data.token, diffMinute * 60000);
          }).catch((err) => {
              console.log("ERR: ", err.response)
          })
      }
  return (
    <>
      <Navbar bg="dark" variant="dark">
        <Container>
          <Navbar.Brand href="/home/article">SimpleWeb</Navbar.Brand>
          <Nav className="me-auto">
            <Link to="/article" className="link-item">
              Article
            </Link>
            <Link to="/profile" className="link-item">
              Profile
            </Link>
          </Nav>
        </Container>
      </Navbar>
      <Container>
        <Outlet context={[refreshToken]}/>
      </Container>
    </>
  );
}

export default NavbarLayout;
