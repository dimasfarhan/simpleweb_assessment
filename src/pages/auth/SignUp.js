import { Buffer } from "buffer";
import React, { useEffect, useState } from "react";
import {
  Button,
  Card,
  Col,
  Container,
  Form,
  FormControl,
  InputGroup,
  Row,
  Tab,
  Tabs,
} from "react-bootstrap";
import Loader from "react-overlay-loader/lib/Loader";
import { useNavigate } from "react-router-dom";
import Swal from "sweetalert2";
import axiosInstance from "../../config/AxiosInstance";

function SignUp() {
  const navigate = useNavigate();
  const [key, setKey] = useState("account");
  const [loading, setLoading] = useState(false);
  const [form, setForm] = useState({
    username: "",
    password: "",
    first_name: "",
    last_name: "",
    telephone: "",
    profile_image: "",
    address: "",
    city: "",
    province: "",
    country: "",
  });
  const [confPassword, setConfPassword] = useState("");

  const getWithExpiry = (key) => {
    const itemStr = localStorage.getItem(key);
    if (!itemStr) {
      return null;
    }
    const item = JSON.parse(itemStr);
    const now = new Date();

    if (now.getTime() > item.expiry) {
      localStorage.removeItem(key);
      return localStorage.removeItem(key);
    }
    return item.value;
  };

  useEffect(() => {
    getWithExpiry("token") !== null && navigate("/article");
  })

  const handleNext = () => {
    if (
      form.first_name !== "" &&
      form.last_name !== "" &&
      form.username !== "" &&
      form.password !== "" &&
      confPassword !== ""
    ) {
      if (form.password === confPassword) {
        setKey("profile");
      } else {
        Swal.fire({
          title: "Warning",
          text: "Password does not match!",
          icon: "warning",
        });
      }
    } else {
      Swal.fire({
        title: "Warning",
        text: "Please fill in all the fields first!",
        icon: "warning",
      });
    }
  };

  const setWithExpiry = (key, value, ttl) => {
    const now = new Date();

    const item = {
      value: value,
      expiry: now.getTime() + ttl,
    };
    localStorage.setItem(key, JSON.stringify(item));
  };

  const handleSubmit = () => {
    if (
      form.profile_image !== "" &&
      form.address !== "" &&
      form.city !== "" &&
      form.province !== "" &&
      form.country !== ""
    ) {
      const data = new FormData();
      data.append("username", form.username);
      data.append("password", form.password);
      data.append("first_name", form.first_name);
      data.append("last_name", form.last_name);
      data.append("telephone", form.telephone);
      data.append("profile_image", form.profile_image);
      data.append("address", form.address);
      data.append("city", form.city);
      data.append("province", form.province);
      data.append("country", form.country);

      Swal.fire({
        title: "Submit Data?",
        text: "Make sure all input is a valid data",
        icon: "question",
        confirmButtonText: "Yes, Submit",
        confirmButtonColor: "#5D8A3D",
        width: "395px",
        denyButtonText: "Cancel",
        showDenyButton: true,
      }).then((rslt) => {
        if (rslt.isConfirmed) {
          setLoading(true);
          axiosInstance()
            .post("register", data)
            .then((json) => {
              const expiry = JSON.parse(
                Buffer.from(json.data.token.split(".")[1], "base64").toString()
              ).exp;
              const date = new Date(expiry * 1000);
              const now = new Date();
              const diffMs = date - now;
              const diffMinute = Math.round(diffMs / 60000);
              setWithExpiry("token", json.data.token, diffMinute * 60000);
              setLoading(false);
              Swal.fire({
                title: "Success",
                text: `Sign Up Succeed`,
                icon: "success",
              }).then(() => {
                navigate("/article");
              });
            })
            .catch((err) => {
              console.log("ERR: ", err.response);
              Swal.fire({
                title: "Failed",
                text: `${err.response.data.detail}`,
                icon: "warning",
              });
              setLoading(false);
            });
        }
      });
    } else {
      Swal.fire({
        title: "Warning",
        text: "Please fill in all the fields first!",
        icon: "warning",
      });
    }
  };  

  return (
    <Container className="signup-main center-screen">
      <Loader fullPage loading={loading} />
      <Card>
        <Card.Header className="item-center">
          <h2>REGISTER</h2>
        </Card.Header>
        <Card.Body>
          <Tabs id="controlled-tab-example" activeKey={key} className="mb-3">
            <Tab eventKey="account" title="Account" disabled>
              <Form
                onSubmit={(e) => {
                  e.preventDefault();
                  handleNext();
                }}
              >
                <Row>
                  <Col sm={12} md={4} lg={4}>
                    <Form.Group className="mb-3" controlId="firstname">
                      <Form.Label>First Name</Form.Label>
                      <Form.Control
                        type="text"
                        placeholder="Firstname..."
                        onChange={(e) =>
                          setForm({ ...form, first_name: e.target.value })
                        }
                      />
                    </Form.Group>
                  </Col>
                  <Col sm={12} md={4} lg={4}>
                    <Form.Group className="mb-3" controlId="lastname">
                      <Form.Label>Last Name</Form.Label>
                      <Form.Control
                        type="text"
                        placeholder="Lastname..."
                        onChange={(e) =>
                          setForm({ ...form, last_name: e.target.value })
                        }
                      />
                    </Form.Group>
                  </Col>
                  <Col sm={12} md={4} lg={4}>
                    <Form.Label>Phone</Form.Label>
                    <InputGroup className="mb-3">
                      <InputGroup.Text id="basic-addon2">+62 </InputGroup.Text>
                      <FormControl
                        placeholder="phonenumber"
                        aria-label="phonenumber"
                        aria-describedby="basic-addon2"
                        type="tel"
                        onInput={(e) =>
                          (e.target.value = e.target.value
                            .replace(/[^0-9.]/g, "")
                            .replace(/(\..*?)\..*/g, "$1"))
                        }
                        onChange={(e) =>
                          setForm({
                            ...form,
                            telephone: "+62" + e.target.value,
                          })
                        }
                      />
                    </InputGroup>
                  </Col>
                </Row>

                <Row>
                  <Col sm={12} md={4} lg={4}>
                    <Form.Group className="mb-3" controlId="username">
                      <Form.Label>Username</Form.Label>
                      <Form.Control
                        type="email"
                        placeholder="Username..."
                        onChange={(e) =>
                          setForm({ ...form, username: e.target.value })
                        }
                      />
                    </Form.Group>
                  </Col>
                  <Col sm={12} md={4} lg={4}>
                    <Form.Group className="mb-3" controlId="password">
                      <Form.Label>Password</Form.Label>
                      <Form.Control
                        type="password"
                        placeholder="Password..."
                        onChange={(e) =>
                          setForm({ ...form, password: e.target.value })
                        }
                      />
                    </Form.Group>
                  </Col>
                  <Col sm={12} md={4} lg={4}>
                    <Form.Group className="mb-3" controlId="confPassword">
                      <Form.Label>Confirmation Password</Form.Label>
                      <Form.Control
                        type="password"
                        placeholder="Re-type Password..."
                        onChange={(e) => setConfPassword(e.target.value)}
                      />
                    </Form.Group>
                  </Col>
                </Row>
              </Form>

              <Row className="signup-footer">
                <Col md={12} className="mt-3">
                  <Button variant="primary" onClick={handleNext}>
                    Next
                  </Button>
                </Col>
              </Row>
            </Tab>
            <Tab eventKey="profile" title="Profile" disabled>
              <Form
                onSubmit={(e) => {
                  e.preventDefault();
                  handleSubmit();
                }}
              >
                <Row>
                  <Col sm={12} md={4} lg={4}>
                    <Form.Group controlId="profileImage" className="mb-3">
                      <Form.Label>Profile Image</Form.Label>
                      <Form.Control
                        type="file"
                        accept="image/png, image/gif, image/jpeg"
                        onChange={(e) =>
                          setForm({ ...form, profile_image: e.target.files[0] })
                        }
                      />
                    </Form.Group>
                  </Col>
                  <Col sm={12} md={8} lg={8}>
                    <Form.Group className="mb-3" controlId="address">
                      <Form.Label>Address</Form.Label>
                      <Form.Control
                        type="text"
                        placeholder="Address..."
                        onChange={(e) =>
                          setForm({ ...form, address: e.target.value })
                        }
                      />
                    </Form.Group>
                  </Col>
                </Row>

                <Row>
                  <Col sm={12} md={4} lg={4}>
                    <Form.Group className="mb-3" controlId="city">
                      <Form.Label>City</Form.Label>
                      <Form.Control
                        type="text"
                        placeholder="City..."
                        onChange={(e) =>
                          setForm({ ...form, city: e.target.value })
                        }
                      />
                    </Form.Group>
                  </Col>
                  <Col sm={12} md={4} lg={4}>
                    <Form.Group className="mb-3" controlId="province">
                      <Form.Label>Province</Form.Label>
                      <Form.Control
                        type="text"
                        placeholder="Province..."
                        onChange={(e) =>
                          setForm({ ...form, province: e.target.value })
                        }
                      />
                    </Form.Group>
                  </Col>
                  <Col sm={12} md={4} lg={4}>
                    <Form.Group className="mb-3" controlId="country">
                      <Form.Label>Country</Form.Label>
                      <Form.Control
                        type="text"
                        placeholder="Country..."
                        onChange={(e) =>
                          setForm({ ...form, country: e.target.value })
                        }
                      />
                    </Form.Group>
                  </Col>
                </Row>
              </Form>

              <Row className="signup-footer">
                <Col md={12} className="mt-3">
                  <Button
                    className="mright-2"
                    variant="danger"
                    onClick={() => setKey("account")}
                  >
                    Back
                  </Button>
                  <Button variant="success" onClick={handleSubmit}>
                    Submit
                  </Button>
                </Col>
              </Row>
            </Tab>
          </Tabs>
        </Card.Body>
      </Card>
    </Container>
  );
}

export default SignUp;
