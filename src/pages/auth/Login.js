import React, { useEffect, useState } from "react";
import { Button, Card, Col, Container, Form, Row } from "react-bootstrap";
import Loader from "react-overlay-loader/lib/Loader";
import { useNavigate } from "react-router-dom";
import Swal from "sweetalert2";
import axiosInstance from "../../config/AxiosInstance";

import "../../style/auth-style.css";

function Login() {
  let navigate = useNavigate();
  const [loading, setLoading] = useState(false);
  const [form, setForm] = useState({
    username: "",
    password: "",
  });

  const getWithExpiry = (key) => {
    const itemStr = localStorage.getItem(key);
    if (!itemStr) {
      return null;
    }
    const item = JSON.parse(itemStr);
    const now = new Date();

    if (now.getTime() > item.expiry) {
      localStorage.removeItem(key);
      return localStorage.removeItem(key);
    }
    return item.value;
  };

  useEffect(() => {
    getWithExpiry("token") !== null && navigate("/article");
  })

  const setWithExpiry = (key, value, ttl) => {
    const now = new Date();

    const item = {
      value: value,
      expiry: now.getTime() + ttl,
    };
    localStorage.setItem(key, JSON.stringify(item));
  };

  const handleLogin = () => {
    if (form.username !== "" && form.password !== "") {
      setLoading(true);
      axiosInstance()
        .post("api/token/", form)
        .then((json) => {
          const expiry = JSON.parse(
            Buffer.from(json.data.token.split(".")[1], "base64").toString()
          ).exp;
          const date = new Date(expiry * 1000);
          const now = new Date();
          const diffMs = date - now;
          const diffMinute = Math.round(diffMs / 60000);
          setWithExpiry("token", json.data.token, diffMinute * 60000);
          setLoading(false);
          Swal.fire({
            title: "Success",
            text: `Sign In Succeed`,
            icon: "success",
          }).then(() => {
            navigate("/article");
          });
          setLoading(false);
        })
        .catch((err) => {
          console.log("ERR: ", err.response);
          Swal.fire({
            title: "Failed",
            text: `${err.response.data.detail}`,
            icon: "warning",
          });
          setLoading(false);
        });
    } else {
      Swal.fire({
        title: "Warning",
        text: "Please fill in all the fields first!",
        icon: "warning",
      });
    }
  };

  const toSignUp = () => {
    navigate("/sign-up");
  };

  return (
    <Container className="login-main center-screen">
      <Loader fullPage loading={loading} />
      <Card className="item-center">
        <Card.Header>
          <h2>LOGIN</h2>
        </Card.Header>
        <Card.Body>
          <Form className="login-body" onSubmit={(e) => e.preventDefault()}>
            <Form.Group className="mb-3" controlId="username">
              <Form.Label>Username</Form.Label>
              <Form.Control
                type="email"
                placeholder="Username..."
                onChange={(e) => setForm({ ...form, username: e.target.value })}
              />
            </Form.Group>
            <Form.Group className="mb-3" controlId="password">
              <Form.Label>Password</Form.Label>
              <Form.Control
                type="password"
                placeholder="Password..."
                onChange={(e) => setForm({ ...form, password: e.target.value })}
              />
            </Form.Group>
          </Form>

          <Row className="login-footer">
            <Col md={12}>
              <span className="to-signup" onClick={toSignUp}>
                Dont have an account yet? Sign up here
              </span>
            </Col>
            <Col md={12} className="mt-3">
              <Button variant="primary" onClick={handleLogin}>
                Login
              </Button>
            </Col>
          </Row>
        </Card.Body>
      </Card>
    </Container>
  );
}

export default Login;
