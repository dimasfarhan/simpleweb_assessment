import React from "react";
import { Card, Pagination } from "react-bootstrap";
import { useOutletContext } from "react-router-dom";

function ArticleList() {
  const [blog, count, limit, offset, truncate, toDetail, setOffset] =
    useOutletContext();
  let page = count % limit;

  const renderPagination = (value) => {
    let elements = [];
    let newOffset = 0;
    for (let index = 0; index < value; index++) {
      let num = index;
      let finalOffset = newOffset;
      let isActive = finalOffset === offset ? true : false;
      num += 1;
      elements.push(
        <Pagination.Item
          key={index}
          active={isActive}
          onClick={() => setOffset(finalOffset)}
        >
          {num}
        </Pagination.Item>
      );
      newOffset += limit;
    }
    return elements;
  };

  return (
    <>
      {blog.map(({ name, content, id }, index) => (
        <Card
          className="mb-3 article-list"
          key={index}
          onClick={() => toDetail(id)}
        >
          <Card.Header className="article-list-head">
            <Card.Title>{name}</Card.Title>
          </Card.Header>
          <Card.Body className="article-list-body">
            <Card.Text>{truncate(content)}</Card.Text>
          </Card.Body>
        </Card>
      ))}

      <Pagination>
        <Pagination.First onClick={() => setOffset(0)} />
        {renderPagination(page)}
        <Pagination.Last onClick={() => setOffset(count - (page - 1))} />
      </Pagination>
    </>
  );
}

export default ArticleList;
