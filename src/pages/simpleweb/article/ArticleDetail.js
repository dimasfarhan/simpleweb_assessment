import React, { useEffect, useState } from "react";
import { Card } from "react-bootstrap";
import Loader from "react-overlay-loader/lib/Loader";
import { useNavigate, useParams } from "react-router-dom";
import Swal from "sweetalert2";
import axiosInstance from "../../../config/AxiosInstance";

function ArticleDetail() {
  const { id } = useParams();
  let navigate = useNavigate();
  //   const [blog, setBlog] = useState({
  //     id: 1,
  //     name: "Hacktivism",
  //     content:
  //       'Origins and definitions\r\nWriter Jason Sack first used the term hacktivism in a 1995 article in conceptualizing New Media artist Shu Lea Cheang\'s film Fresh Kill.[5][6] However, the term is frequently attributed to the Cult of the Dead Cow (cDc) member "Omega," who used it in a 1996 e-mail to the group.[7][8] Due to the variety of meanings of its root words, the definition of hacktivism is nebulous and there exists significant disagreement over the kinds of activities and purposes it encompasses. Some definitions include acts of cyberterrorism while others simply reaffirm the use of technological hacking to effect social change.[9][10]\r\n\r\nForms and methods\r\nSelf-proclaimed "hactivists" often work anonymously, sometimes operating in groups while other times operating as a lone-wolf with several cyber-personas all corresponding to one activist[11] within the cyberactivism umbrella that has been gaining public interest and power in pop-culture. Hactivists generally operate under apolitical ideals and express uninhibited ideas or abuse without being scrutinized by society while representing or defending them publicly under an anonymous identity giving them a sense of power in the cyberactivism community[citation needed].\r\n\r\nIn order to carry out their operations, hacktivists might create new tools; or integrate or use a variety of software tools readily available on the Internet. One class of hacktivist activities includes increasing the accessibility of others to take politically motivated action online[citation needed].\r\n\r\nRepertoire of contention of hacktivism includes among others:\r\n\r\nCode: Software and websites can achieve political goals. For example, the encryption software PGP can be used to secure communications; PGP\'s author, Phil Zimmermann said he distributed it first to the peace movement.[12] Jim Warren suggests PGP\'s wide dissemination was in response to Senate Bill 266, authored by Senators Biden and DeConcini, which demanded that "...communications systems permit the government to obtain the plain text contents of voice, data, and other communications...".[13] WikiLeaks is an example of a politically motivated website: it seeks to "keep governments open".[14]\r\nMirroring. Website mirroring is used as a circumvention tool in order to bypass various censorship blocks on websites. This technique copies the contents of a censored website and disseminates it on other domains and sub-domains that are not censored.[15] Document mirroring, similar to website mirroring, is a technique that focuses on backing up various documents and other works. RECAP is software that was written with the purpose to \'liberate US case law\' and make it openly available online. The software project takes the form of distributed document collection and archival.[16] Major mirroring projects include initiatives such as the Internet Archive and Wikisource.\r\nAnonymity: a method of speaking out to a wide audience about human rights issues, government oppression, etc. that utilizes various web tools such as free and/or disposable email accounts, IP masking, and blogging software to preserve a high level of anonymity.[17]\r\nDoxing: The practice in which private and/or confidential documents and records are hacked into and made public. Hacktivists view this as a form of assured transparency, experts claim it is harassment.[18]\r\nDenial-of-service attacks: These attacks, commonly referred to as DoS attacks, use large arrays of personal and public computers that hackers take control of via malware executable files usually transmitted through email attachments or website links. After taking control, these computers act like a herd of zombies, redirecting their network traffic to one website, with the intention of overloading servers and taking a website offline.[18]\r\nVirtual sit-ins: Similar to DoS attacks but executed by individuals rather than software, a large number of protesters visit a targeted website and rapidly load pages to overwhelm the site with network traffic to slow the site or take it offline.[19]\r\nWebsite defacements: Hackers infiltrate a web server to replace a specific web page with one of their own, usually to convey a specific message.[20][19]\r\nWebsite redirects:This method involves changing the address of a website within the server so would-be visitors of the site are redirected to a site created by the perpetrator, typically to denounce the original site.[19]\r\nGeo-bombing: a technique in which netizens add a geo-tag while editing YouTube videos so that the location of the video can be seen in Google Earth.[21]',
  //   });

  const [blog, setBlog] = useState({});
  const [loading, setLoading] = useState(true);

  useEffect(() => {
    axiosInstance()
      .get(`article/${id}`)
      .then((json) => {
        setBlog(json.data);
        setLoading(false);
      })
      .catch((err) => {
        console.log("ERR: ", err.response);
        setLoading(false);
        if (err.response.status === 401) {
            Swal.fire({
              title: "Warning",
              text: "Your session is expired! Please login first.",
              icon: "error",
            }).then(() => {
              localStorage.clear();
              navigate("/login");
            });
          }
      });
  }, [id]);

  return (
    <>
      <Loader fullPage loading={loading} />

      <Card className="mb-3">
        <Card.Header className="article-list-head">
          <Card.Title>{blog.name}</Card.Title>
        </Card.Header>
        <Card.Body className="article-list-body">
          <Card.Text>{blog.content}</Card.Text>
        </Card.Body>
      </Card>
    </>
  );
}

export default ArticleDetail;
