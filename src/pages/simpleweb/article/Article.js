import React, { useCallback, useEffect, useState } from "react";
import { Card, Container } from "react-bootstrap";
import Loader from "react-overlay-loader/lib/Loader";
import { Outlet, useNavigate, useOutletContext } from "react-router-dom";
import Swal from "sweetalert2";
import axiosInstance from "../../../config/AxiosInstance";

function Article() {
  let navigate = useNavigate();
  const [refreshToken] = useOutletContext();
  const [count, setCount] = useState(13);
  const [limit, setLimit] = useState(5);
  const [offset, setOffset] = useState(0);
  const [blog, setBlog] = useState([]);
  const [loading, setLoading] = useState(false);

  useEffect(() => {
    setLoading(true);
    refreshToken();
    loadBlog();
  }, [offset, setOffset]);

  const loadBlog = () => {
    axiosInstance()
      .get(`article`, {
        params: {
          limit: limit,
          offset: offset,
        },
      })
      .then((json) => {
        setCount(json.data.count);
        setBlog(json.data.results);
        //   console.log("RESULT: ", json.data.results)
        setLoading(false);
      })
      .catch((err) => {
        console.log("ERR: ", err.response);
        setLoading(false);
        if (err.response.status === 401) {
          Swal.fire({
            title: "Warning",
            text: "Your session is expired! Please login first.",
            icon: "error",
          }).then(() => {
            localStorage.clear();
            navigate("/login");
          });
        }
      });
  };

  const toDetail = (id) => {
    navigate(`/article/detail/${id}`);
  };

  const truncate = (str) => {
    return str.length > 25 ? str.substring(0, 250) + "..." : str;
  };

  return (
    <Container className="m-3">
      <Loader fullPage loading={loading} />

      <h2 className="mb-3">Article</h2>
      <Outlet
        context={[
          //   BlogPosts,
          blog,
          count,
          limit,
          offset,
          truncate,
          toDetail,
          setOffset,
        ]}
      />
    </Container>
  );
}

export default Article;
