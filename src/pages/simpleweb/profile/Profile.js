import React, { useEffect, useState } from "react";
import { Card, Col, Container, Image, Row } from "react-bootstrap";
import Loader from "react-overlay-loader/lib/Loader";
import { useNavigate, useOutletContext } from "react-router-dom";
import Swal from "sweetalert2";
import axiosInstance from "../../../config/AxiosInstance";

function Profile() {
  let navigate = useNavigate();
  const [refreshToken] = useOutletContext();
  const [user, setUser] = useState({});
  const [loading, setLoading] = useState(false);

  useEffect(() => {
    refreshToken();
    axiosInstance()
      .get(`profile`)
      .then((json) => {
        setUser(json.data);
        setLoading(false);
      })
      .catch((err) => {
        console.log("ERR: ", err.response);
        setLoading(false);
        if (err.response.status === 401) {
          Swal.fire({
            title: "Warning",
            text: "Your session is expired! Please login first.",
            icon: "error",
          }).then(() => {
            localStorage.clear();
            navigate("/login");
          });
        }
      });
  });

  return (
    <Container className="m-3">
      <Loader fullPage loading={loading} />
      <h2 className="mb-3">Profile</h2>
      <Card>
        <Card.Header>
          <Card.Title>User Detail</Card.Title>
        </Card.Header>
        <Card.Body className="m-4">
          <Row>
            <Col sm={12} md={4} lg={4}>
              <Image
                src={"http://13.212.226.116:8000" + user.profile_image}
                thumbnail
              />
            </Col>
            <Col sm={12} md={8} lg={8}>
              <Row>
                <Col sm={2} md={2}>
                  <Card.Text>Name</Card.Text>
                </Col>
                <Col sm={10} md={10}>
                  <Card.Text>
                    :{" "}
                    {user.user &&
                      user.user.first_name + " " + user.user.last_name}
                  </Card.Text>
                </Col>
              </Row>
              <Row>
                <Col sm={2} md={2}>
                  <Card.Text>Email</Card.Text>
                </Col>
                <Col sm={10} md={10}>
                  <Card.Text>: {user.user && user.user.username}</Card.Text>
                </Col>
              </Row>
              <Row>
                <Col sm={2} md={2}>
                  <Card.Text>Telephone</Card.Text>
                </Col>
                <Col sm={10} md={10}>
                  <Card.Text>: {user.telephone}</Card.Text>
                </Col>
              </Row>
              <Row>
                <Col sm={2} md={2}>
                  <Card.Text>Address</Card.Text>
                </Col>
                <Col sm={10} md={10}>
                  <Card.Text>: {user.address}</Card.Text>
                </Col>
              </Row>
              <Row>
                <Col sm={2} md={2}>
                  <Card.Text>City</Card.Text>
                </Col>
                <Col sm={10} md={10}>
                  <Card.Text>: {user.city}</Card.Text>
                </Col>
              </Row>
              <Row>
                <Col sm={2} md={2}>
                  <Card.Text>Province</Card.Text>
                </Col>
                <Col sm={10} md={10}>
                  <Card.Text>: {user.province}</Card.Text>
                </Col>
              </Row>
              <Row>
                <Col sm={2} md={2}>
                  <Card.Text>Country</Card.Text>
                </Col>
                <Col sm={10} md={10}>
                  <Card.Text>: {user.country}</Card.Text>
                </Col>
              </Row>
            </Col>
          </Row>
        </Card.Body>
      </Card>
    </Container>
  );
}

export default Profile;
