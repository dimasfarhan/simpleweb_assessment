import React, { useEffect } from "react";
import { Container } from "react-bootstrap";
import { useNavigate, useParams } from "react-router-dom";
import Article from "./article/Article";
import Profile from "./profile/Profile";

function WebHome() {
  let param = useParams();
  let navigate = useNavigate();
  useEffect(() => {
    // getWithExpiry("token") === null && navigate("/");
    if (param.page !== "article" && param.page !== "profile") {
      navigate("/");
    }
  });

  const getWithExpiry = (key) => {
    const itemStr = localStorage.getItem(key);
    if (!itemStr) {
      return null;
    }
    const item = JSON.parse(itemStr);
    const now = new Date();

    if (now.getTime() > item.expiry) {
      localStorage.removeItem(key);
      return localStorage.removeItem(key);
    }
    return item.value;
  };

  const renderPage = () => {
    switch (param.page) {
      case "article":
        return <Article />;

      case "profile":
        return <Profile />;

      default:
        return <Article />;
    }
  };

  return (
    <>
      <Container>{renderPage()}</Container>
    </>
  );
}

export default WebHome;
