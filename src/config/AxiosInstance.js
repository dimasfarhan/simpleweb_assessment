import axios from 'axios';

function getWithExpiry(key) {
    const itemStr = localStorage.getItem(key);
    if (!itemStr) {
        return null;
    }
    const item = JSON.parse(itemStr);
    const now = new Date();

    if (now.getTime() > item.expiry) {
        localStorage.removeItem(key);
        return localStorage.removeItem(key);
    }
    return item.value;
}

const axiosInstance = (additionalConfig = {}) => {
    const value = getWithExpiry('token');
    return axios.create({
        baseURL: 'https://cors-anywhere.herokuapp.com/http://13.212.226.116:8000/api/', 
        // baseURL: 'http://13.212.226.116:8000/api/', 
        headers: {
            authorization: `TSTKRI ${value}`,
            'Content-Type': 'application/json',
            'Access-Control-Allow-Origin': true,
        },
        ...additionalConfig,
    });
};

export default axiosInstance;