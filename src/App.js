import React from "react";
import { BrowserRouter, Navigate, Route, Routes } from "react-router-dom";
import NavbarLayout from "./layout/NavbarLayout";
import Login from "./pages/auth/Login";
import SignUp from "./pages/auth/SignUp";
import Article from "./pages/simpleweb/article/Article";
import ArticleDetail from "./pages/simpleweb/article/ArticleDetail";
import ArticleList from "./pages/simpleweb/article/ArticleList";
import Profile from "./pages/simpleweb/profile/Profile";
import "./style/main-style.css";

function App() {
  return (
    <BrowserRouter>
      <Routes>
        <Route path="/login" element={<Login />} />
        <Route path="/" element={<Navigate replace to="/login" />} />
        <Route path="/sign-up" element={<SignUp />} />
        <Route element={<NavbarLayout />}>
          <Route path="/article" element={<Article />}>
            <Route path="" element={<ArticleList />} />
            <Route path="detail/:id" element={<ArticleDetail />} />
          </Route>
          <Route path="/profile" element={<Profile />} />
          <Route path="*" element={<Navigate replace to="/article/" />} />
        </Route>
      </Routes>
    </BrowserRouter>
  );
}

export default App;
