# How To Run

1. yarn install
2. open 'https://cors-anywhere.herokuapp.com/corsdemo' to bypass cors from server
3. yarn start


# NOTE

1. Cannot login, API cannot be accessed
2. Cannot refresh token, API cannot be accessed